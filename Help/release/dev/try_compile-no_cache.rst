try_compile-no_cache
--------------------

* The :command:`try_compile` and :command:`try_run` commands gained the option
  ``NO_CACHE`` to store results in normal variables.
