vs11-deprecate
--------------

* The :generator:`Visual Studio 11 2012` generator is now deprecated
  and will be removed in a future version of CMake.
